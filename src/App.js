import React from 'react';
import './App.css';
import NavBar from "./components/NavBar/NavBar";
import {BrowserRouter, Route, withRouter} from "react-router-dom";
import UsersContainer from "./components/Users/UsersContainer";
import HeaderContainer from "./components/Header/HeaderContainer";
import Login from "./components/Login/Login";
import {connect, Provider} from "react-redux";
import {compose} from "redux";
import {initializeApp} from "./redux/app-reducer";
import Preloader from "./components/common/Preloader/Preloader";
import store from "./redux/redux-store";
import {withSuspense} from "./hoc/withSuspense";
import Switch from "react-router-dom/Switch";
import Redirect from "react-router-dom/Redirect";

const DialogsContainer = React.lazy(() => import(`./components/Dialogs/DialogsContainer`));
const ProfileContainer = React.lazy(() => import(`./components/Profile/ProfileContainer`));

class App extends React.Component {
    // catchAllUnhandledErrors = (promiseRejectionEvent) => {
    //     alert("Some error occurred");
    //     console.error(promiseRejectionEvent);
    // }
    componentDidMount() {
        this.props.initializeApp();
        // window.addEventListener("unhandledrejection",
        //     this.catchAllUnhandledErrors);
    }
    // componentWillUnmount() {
    //     window.removeEventListener("unhandledrejection",
    //         this.catchAllUnhandledErrors);
    // }

    render() {
        if (!this.props.initialized) {
            return <Preloader/>
        }
        return (
            //app-wrapper но так почему то не работает
            <div className='app-wripper'>
                <HeaderContainer/>
                <NavBar/>
                <div className='app-wrapper-content'>
                    <Switch>
                        <Route exact path='/'
                               render={() => <Redirect to={"/profile"}/>}/>

                        <Route path='/dialogs'
                               render={withSuspense(DialogsContainer)}/>

                        <Route path='/profile/:userId?'
                               render={withSuspense(ProfileContainer)}/>

                        <Route path='/users' render={() =>
                            <UsersContainer/>}/>

                        <Route path='/login' render={() =>
                            <Login/>}/>

                        <Route path='*' render={() =>
                            <div>404 Not found.</div>}/>
                    </Switch>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    initialized: state.app.initialized
})

// export default compose(
//     withRouter,
//     connect(mapStateToProps, {initializeApp}))
// (App);
let AppContainer = compose(
    withRouter,
    connect(mapStateToProps, {initializeApp}))
(App);

const SamuraiJsApp = (props) => {
    return <BrowserRouter basename={process.env.PUBLIC_URL}>
        <Provider store={store}>
            <AppContainer/>
        </Provider>
    </BrowserRouter>
}

export default SamuraiJsApp;


