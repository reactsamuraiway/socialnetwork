import React from 'react';
import profileReducer, {addPostActionCreator, deletePost} from "./profile-reducer";

let state = {
    posts: [
        {id: 1, message: 'Post 1', likesCount: 12},
        {id: 2, message: 'Post 2', likesCount: 4},
        {id: 3, message: 'Post 3', likesCount: 8}
    ]
};

it('length of post should be incremented', () => {
    let action = addPostActionCreator("bsv.com")

    let newState = profileReducer(state, action)

    expect(newState.posts.length).toBe(4)
});

it('message of new post should be bsv.com', () => {
    let action = addPostActionCreator("bsv.com")

    let newState = profileReducer(state, action)

    expect(newState.posts[3].message).toBe("bsv.com")
});

it('after deleting length of post should be decremented', () => {
    let action = deletePost(1)

    let newState = profileReducer(state, action)

    expect(newState.posts.length).toBe(2)
});

it(`after deleting length of post shouldn't be decremented if id is incorrect`, () => {
    let action = deletePost(1000)

    let newState = profileReducer(state, action)

    expect(newState.posts.length).toBe(3)
});