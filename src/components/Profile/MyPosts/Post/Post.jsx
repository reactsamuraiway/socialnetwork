import React from 'react';
import s from './Post.module.css';

const Post = (props) => {
    return (
        <div>
            <div className={s.item}>
                <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTp8vh9I4qPrwIWLdY9JpU8ZaFTrNtlgvRKwA&usqp=CAU'/>
                {props.message}
            </div>
            <div><span>Like {props.likesCount}</span></div>
        </div>
    )
}

export default Post;