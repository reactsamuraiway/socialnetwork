import React from 'react';
import {maxLengthCreator, required} from "../../../utils/validators/validators";
import {Textarea} from "../../common/FormsControls/FormsControls";
import Field from "redux-form/lib/Field";
import reduxForm from "redux-form/lib/reduxForm";

const maxLength15 = maxLengthCreator(15);

const AddMessageForm = (props) => {
    return (
        <form onSubmit={props.handleSubmit}>
            <div>
                <Field component={Textarea}
                       validate={[required, maxLength15]}
                       name="newMessageBody"
                       placeholder="Enter your message" />
            </div>
            <div><button >Send</button></div>
        </form>
    )
}


const AddMessageFormRedux = reduxForm({form: "dialogAddMessageForm"})(AddMessageForm);

export default AddMessageFormRedux;